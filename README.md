# Bleeding edge Repo hello world

a configuration setup to create and configure a rpm/deb repository as demo for [deployctl](https://www.deployctl.com)

repository at: [http://production.deployctl-bleeding-repo-hello-world.gioxapp.com](http://production.deployctl-bleeding-repo-hello-world.gioxapp.com)

## the gitlab-ci.yaml

```yaml
stages:
  - production

variables:
  CI_PROJECT_PATH_SLUG: "deployctl-bleeding-repo-hello-world"
  DEPLOY_DOMAIN_APP: "gioxapp.com"
  DEPLOY_REPO_NAME: "bleeding_repo_hello_world"


production:
  stage: production
  tags: 
  - deployctl-gioxapp.com
  environment:
    name: production
    url: http://$CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
  script:
    - deployctl repo_config

```

`deployctl repo_config` needs some variable to be setup:

*  Environment should be production
*  `CI_PROJECT_PATH_SLUG` needs to be defined since no custom domain in `DEPLOY_DOMAIN` is defined.
   Since gitlab 9.3, this is defined, but contains `_` making it a no go to use as part of a domain name since let's encrypt does not accept `_`. [#34643](https://gitlab.com/gitlab-org/gitlab-ce/issues/34643)
* `DEPLOY_DOMAIN_APP` is the domain where deployctl is running on, used to create repo usrl.
*  `DEPLOY_REPO_NAME` the name of the repo, this name will be the name on the client install repository.

## the description.md

this file is rendered into html to display on the repository landing page, and some substitutions are make:

* `@@n@@` renders to `DEPLOY_REPO_NAME`
* `@@u@@` renders to [http://production.deployctl-bleeding-repo-hello-world.gioxapp.com](http://production.deployctl-bleeding-repo-hello-world.gioxapp.com)

Main purpose is to inform users how to install the repository.

`deployctl` generates 2 scripts for repository setup:
  *  `repo.deb.sh` for debian based systems
  *  `repo.rpm.sh` for rpm based systems

## repo.yaml

this is the actual configuration of the pepository:

`projects:` list of projects that are allowed to add packages to this repository

`environments:` lists the environments that are allowed to add packages to this repository
    *  wildcards allowed for `review/*`
    *  `all` all environments are allowed to push
    *  `none` only non environments are allowed to push
    *   if `environments:` is empty or not defined, defaults to `production`


`repos:` the repository definition of OS - Dist / arch



  
